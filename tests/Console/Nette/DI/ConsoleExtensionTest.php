<?php

declare(strict_types=1);

namespace Interitty\Console\Nette\DI;

use Interitty\Console\Application;
use Interitty\Console\BaseCommand;
use Interitty\Console\LazyCommandLoader;
use Interitty\PhpUnit\BaseIntegrationTestCase;
use Nette\DI\ServiceCreationException;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Console\Nette\DI\ConsoleExtension
 */
class ConsoleExtensionTest extends BaseIntegrationTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of Lazy command loader setup for AsCommand attribute implementation
     *
     * @return void
     * @covers ::processConfig
     * @covers ::setupConsoleApplication
     * @covers ::setupLazyComandLoader
     * @covers ::getComandMap
     * @covers ::getCommandAttributeName
     * @group integration
     */
    public function testSetupLazyComandLoaderAttribute(): void
    {
        $commandClassName = 'DummyCommand';
        $commandName = 'dummy:attribute-name';
        $code = '<?php
            #[\Symfony\Component\Console\Attribute\AsCommand(name: \'' . $commandName . '\')]
            class ' . $commandClassName . ' extends ' . BaseCommand::class . ' {
            public function processExecute(): int { return self::SUCCESS; }
        }';
        $this->processRegisterAutoload($commandClassName, $code);
        $configContent = '
extensions:
    console: Interitty\Console\Nette\DI\ConsoleExtension(%consoleMode%)
services:
    - ' . $commandClassName;
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);

        /** @var Application $application */
        $application = $container->getByType(Application::class);
        $loader = $this->getNonPublicPropertyValue($application, 'commandLoader');
        self::assertInstanceOf(LazyCommandLoader::class, $loader);
        self::assertTrue($application->has($commandName));
        self::assertInstanceOf(BaseCommand::class, $application->get($commandName));
    }

    /**
     * Tester of Lazy command loader setup for missing name implementation
     *
     * @return void
     * @covers ::processConfig
     * @covers ::setupLazyComandLoader
     * @covers ::getComandMap
     * @covers ::getCommandAttributeName
     * @group negative
     */
    public function testSetupLazyComandLoaderException(): void
    {
        $commandClassName = 'ExceptionCommand';
        $code = '<?php
            class ' . $commandClassName . ' extends ' . BaseCommand::class . ' {
            public function processExecute(): int { return self::SUCCESS; }
        }';
        $this->processRegisterAutoload($commandClassName, $code);
        $configContent = '
extensions:
    console: Interitty\Console\Nette\DI\ConsoleExtension(%consoleMode%)
services:
    - ' . $commandClassName;
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $this->expectException(ServiceCreationException::class);
        $this->expectExceptionMessage('Command ":class" missing #[AsCommand] attribute');
        $this->expectExceptionData(['class' => $commandClassName]);
        $this->createContainer($configFile);
    }

    /**
     * Tester of Lazy command loader setup for no console mode implementation
     *
     * @return void
     * @covers ::processConfig
     * @covers ::setupLazyComandLoader
     * @covers ::getComandMap
     * @covers ::getCommandAttributeName
     * @group integration
     */
    public function testSetupLazyComandLoaderNoConsoleMode(): void
    {
        $configContent = '
extensions:
    console: Interitty\Console\Nette\DI\ConsoleExtension(false)';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);
        self::assertEmpty($container->findByType(Application::class));
        self::assertEmpty($container->findByType(LazyCommandLoader::class));
    }

    /**
     * Tester of Lazy command loader setup for tag name implementation
     *
     * @return void
     * @covers ::processConfig
     * @covers ::setupConsoleApplication
     * @covers ::setupLazyComandLoader
     * @covers ::getComandMap
     * @covers ::getCommandTagName
     * @group integration
     */
    public function testSetupLazyComandLoaderTag(): void
    {
        $commandClassName = 'TagCommand';
        $commandName = 'dummy:tag-name';
        $code = '<?php
            class ' . $commandClassName . ' extends ' . BaseCommand::class . ' {
            public function processExecute(): int { return self::SUCCESS; }
        }';
        $this->processRegisterAutoload($commandClassName, $code);
        $configContent = '
extensions: {console: Interitty\Console\Nette\DI\ConsoleExtension(%consoleMode%)}
services:
    dummy:
        class: ' . $commandClassName . '
        tags: [console.command: ' . $commandName . ']';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);
        /** @var Application $application */
        $application = $container->getByType(Application::class);
        $loader = $this->getNonPublicPropertyValue($application, 'commandLoader');
        self::assertInstanceOf(LazyCommandLoader::class, $loader);
        self::assertTrue($application->has($commandName));
        self::assertInstanceOf(BaseCommand::class, $application->get($commandName));
    }

    /**
     * Tester of Lazy command loader setup for tag array name implementation
     *
     * @return void
     * @covers ::processConfig
     * @covers ::setupLazyComandLoader
     * @covers ::getComandMap
     * @covers ::getCommandTagName
     * @group integration
     */
    public function testSetupLazyComandLoaderTagArray(): void
    {
        $commandClassName = 'TagCommand';
        $commandName = 'dummy:tag-array-name';
        $code = '<?php
            class ' . $commandClassName . ' extends ' . BaseCommand::class . ' {
            public function processExecute(): int { return self::SUCCESS; }
        }';
        $this->processRegisterAutoload($commandClassName, $code);
        $configContent = '
extensions:
    console: Interitty\Console\Nette\DI\ConsoleExtension(%consoleMode%)
services:
    dummy:
        class: ' . $commandClassName . '
        tags: [console.command: {name: ' . $commandName . '}]';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);

        /* @var $loader LazyCommandLoader */
        $loader = $container->getByType(LazyCommandLoader::class);
        self::assertContains($commandName, $loader->getNames());
        self::assertTrue($loader->has($commandName));
        self::assertInstanceOf(BaseCommand::class, $loader->get($commandName));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of beforeCompile
     *
     * @return void
     * @covers ::beforeCompile
     */
    public function testBeforeCompile(): void
    {
        $extension = $this->createConsoleExtensionMock([
            'isConsoleMode',
            'setupConsoleApplication',
            'setupLazyComandLoader',
        ]);
        $extension->expects(self::once())
            ->method('isConsoleMode')->willReturn(true);
        $extension->expects(self::once())
            ->method('setupConsoleApplication');
        $extension->expects(self::once())
            ->method('setupLazyComandLoader');
        $extension->beforeCompile();
    }

    /**
     * Tester of beforeCompile for no console mode
     *
     * @return void
     * @covers ::beforeCompile
     */
    public function testBeforeCompileNoConsoleMode(): void
    {
        $extension = $this->createConsoleExtensionMock([
            'isConsoleMode',
            'setupConsoleApplication',
            'setupLazyComandLoader',
        ]);
        $extension->expects(self::once())
            ->method('isConsoleMode')->willReturn(false);
        $extension->expects(self::never())
            ->method('setupConsoleApplication');
        $extension->expects(self::never())
            ->method('setupLazyComandLoader');
        $extension->beforeCompile();
    }

    /**
     * Tester of ConsoleMode checker/setter implementation
     *
     * @return void
     * @covers ::isConsoleMode
     * @covers ::setConsoleMode
     */
    public function testCheckSetConsoleMode(): void
    {
        $this->processTestGetSetBool(ConsoleExtension::class, 'consoleMode', false);
        $this->processTestGetSetBool(ConsoleExtension::class, 'consoleMode', true);
    }

    /**
     * Tester of Constructor implementation
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstructor(): void
    {
        $consoleMode = true;
        $extension = $this->createConsoleExtensionMock(['setConsoleMode']);
        $extension->expects(self::once())->method('setConsoleMode')->with(self::equalTo($consoleMode));
        $extension->__construct($consoleMode);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * ConsoleExtension mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return ConsoleExtension&MockObject
     */
    protected function createConsoleExtensionMock(array $methods = []): ConsoleExtension&MockObject
    {
        $mock = $this->createPartialMock(ConsoleExtension::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
