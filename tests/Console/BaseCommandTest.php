<?php

declare(strict_types=1);

namespace Interitty\Console;

use DateTimeZone;
use Exception;
use Generator;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\Utils\DateTime;
use LogicException;
use Nette\Application\AbortException;
use Nette\Utils\Json;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use function hash;

/**
 * @coversDefaultClass Interitty\Console\BaseCommand
 */
class BaseCommandTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of DateTimeNow factory implementation
     *
     * @return void
     * @covers ::createDateTimeNow
     * @group integration
     */
    public function testDateTimeNow(): void
    {
        $baseCommand = $this->createBaseCommandMock();
        $now = $this->createDateTime();
        $nowFirst = $this->callNonPublicMethod($baseCommand, 'createDateTimeNow');
        $nowSecond = $this->callNonPublicMethod($baseCommand, 'createDateTimeNow');
        self::assertInstanceOf(DateTime::class, $nowFirst); // @phpstan-ignore staticMethod.alreadyNarrowedType
        self::assertInstanceOf(DateTime::class, $nowSecond); // @phpstan-ignore staticMethod.alreadyNarrowedType
        self::assertNotSame($nowFirst, $nowSecond);
        $firstDifference = $nowFirst->format('U') - $now->format('U');
        $secondDifference = $nowSecond->format('U') - $now->format('U');
        self::assertGreaterThanOrEqual(0, $firstDifference);
        self::assertGreaterThanOrEqual(0, $secondDifference);
        self::assertLessThanOrEqual(5, $firstDifference);
        self::assertLessThanOrEqual(5, $secondDifference);
    }

    /**
     * Tester of LockName factory implementation
     *
     * @return void
     * @covers ::createLockName
     * @group integration
     */
    public function testCreateLockName(): void
    {
        $name = 'name';
        $options = ['foo' => 'bar'];
        $expectedLockName = hash('md5', Json::encode(['name' => $name, 'options' => $options]));
        $input = $this->createInputMock(['getOptions']);
        $input->expects(self::once())->method('getOptions')->willReturn($options);
        $baseCommand = $this->createBaseCommandMock(['getInput', 'getName']);
        $baseCommand->expects(self::once())->method('getName')->willReturn($name);
        $baseCommand->expects(self::once())->method('getInput')->willReturn($input);
        self::assertSame($expectedLockName, $this->callNonPublicMethod($baseCommand, 'createLockName'));
    }

    /**
     * Tester of check long running commands processor implementation
     *
     * @return void
     * @covers ::processCheckLongRunning
     * @group integration
     */
    public function testProcessCheckLongRunning(): void
    {
        $now = $this->createDateTime();
        $startAt = $this->createDateTime();
        $baseCommand = $this->createBaseCommandMock(['createDateTimeNow', 'getStartAt']);
        $baseCommand->expects(self::once())->method('createDateTimeNow')->willReturn($now);
        $baseCommand->expects(self::once())->method('getStartAt')->willReturn($startAt);
        $this->callNonPublicMethod($baseCommand, 'processCheckLongRunning');
    }

    /**
     * Tester of check long running commands processor for exception handle implementation
     *
     * @return void
     * @covers ::processCheckLongRunning
     * @group negative
     */
    public function testProcessCheckLongRunningException(): void
    {
        $now = $this->createDateTime();
        $startAt = $this->createDateTime()->modify('-1 day');
        $baseCommand = $this->createBaseCommandMock(['createDateTimeNow', 'getStartAt']);
        $baseCommand->expects(self::once())->method('createDateTimeNow')->willReturn($now);
        $baseCommand->expects(self::once())->method('getStartAt')->willReturn($startAt);

        $this->expectException(AbortException::class);
        $this->expectExceptionMessage('Command was started yesterday');
        $this->callNonPublicMethod($baseCommand, 'processCheckLongRunning');
    }

    /**
     * DataProvider for tester of log messages processor implementation
     *
     * @return Generator<string, array{0: string|iterable<string>, 1: string|iterable<string>}>
     */
    public function processLogMessagesDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="string message">
        yield 'string message' => ['message', 'message'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="array messages">
        yield 'array messages' => [['message one', 'message two'], ['message one', 'message two']];
        // </editor-fold>
    }

    /**
     * Tester of log messages processor implementation
     *
     * @param string|iterable<string> $messages
     * @param string|iterable<string> $expected
     * @return void
     * @covers ::processLogMessages
     * @dataProvider processLogMessagesDataProvider
     * @group integration
     */
    public function testProcessLogMessages(string|iterable $messages, string|iterable $expected): void
    {
        $baseCommand = $this->createBaseCommandMock();
        $this->setNonPublicPropertyValue($baseCommand, 'isLogTimestamped', false);
        $this->callNonPublicMethod($baseCommand, 'processLogMessages', [&$messages]);
        self::assertEquals($expected, $messages);
    }

    /**
     * DataProvider for tester of log messages extended processor implementation
     *
     * @return Generator<string, array{0: DateTime, 1: string|iterable<string>, 2: string|iterable<string>}>
     */
    public function processLogMessagesExtendedDataProvider(): Generator
    {
        $now = $this->createDateTime();
        $timestamp = '<fg=gray>' . $now->format('Y-m-d h:i:s') . '</> ';
        // <editor-fold defaultstate="collapsed" desc="string message">
        yield 'string message' => [
            $now,
            'message',
            [$timestamp . 'message'],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="array messages">
        yield 'array messages' => [
            $now,
            ['message one', 'message two'],
            [$timestamp . 'message one', $timestamp . 'message two'],
        ];
        // </editor-fold>
    }

    /**
     * Tester of log messages extended processor implementation
     *
     * @param DateTime $now
     * @param string|iterable<string> $messages
     * @param string|iterable<string> $expected
     * @return void
     * @covers ::processLogMessages
     * @dataProvider processLogMessagesExtendedDataProvider
     * @group integration
     */
    public function testProcessLogMessagesExtended(
        DateTime $now,
        string|iterable $messages,
        string|iterable $expected
    ): void {
        $output = $this->createOutputMock(['isDebug']);
        $output->expects(self::once())->method('isDebug')->willReturn(true);
        $baseCommand = $this->createBaseCommandMock(['createDateTimeNow', 'getOutput']);
        $baseCommand->expects(self::once())->method('getOutput')->willReturn($output);
        $baseCommand->expects(self::once())->method('createDateTimeNow')->willReturn($now);
        $this->setNonPublicPropertyValue($baseCommand, 'isLogTimestamped', true);
        $this->callNonPublicMethod($baseCommand, 'processLogMessages', [&$messages]);
        self::assertEquals($expected, $messages);
    }

    /**
     * Tester of ProgressBar factory implementation
     *
     * @return void
     * @covers ::createProgressBar
     * @group integration
     */
    public function testCreateProgressBar(): void
    {
        $output = $this->createOutputMock();
        $baseCommand = $this->createBaseCommandMock(['getOutput']);
        $baseCommand->expects(self::exactly(2))->method('getOutput')->willReturn($output);

        $max = 42;
        $minSeconds = 1 / 50;

        /** @var ProgressBar $first */
        $first = $this->callNonPublicMethod($baseCommand, 'createProgressBar', [$max, $minSeconds]);
        self::assertSame($max, $this->getNonPublicPropertyValue($first, 'max'));
        self::assertSame($minSeconds, $this->getNonPublicPropertyValue($first, 'minSecondsBetweenRedraws'));
        /** @var ProgressBar $second */
        $second = $this->callNonPublicMethod($baseCommand, 'createProgressBar', [$max, $minSeconds]);

        self::assertNotSame($first, $second);
    }

    /**
     * Tester of processException implementation
     *
     * @return void
     * @group integration
     * @covers ::processException
     * @group integration
     */
    public function testProcessException(): void
    {
        $debug = false;
        $message = 'message';
        $exception = new Exception($message);
        $errorOutput = $this->createOutputMock(['isDebug']);
        $errorOutput->expects(self::once())->method('isDebug')->willReturn($debug);
        $baseCommand = $this->createBaseCommandMock(['getErrorOutput', 'writeError']);
        $baseCommand->expects(self::any())->method('getErrorOutput')->willReturn($errorOutput);
        $baseCommand->expects(self::once())->method('writeError')->with(self::equalTo([$message]));
        $result = $this->callNonPublicMethod($baseCommand, 'processException', [$exception]);
        self::assertEquals(BaseCommand::FAILURE, $result);
    }

    /**
     * Tester of processException for AbortException implementation
     *
     * @return void
     * @group integration
     * @covers ::processException
     * @group integration
     */
    public function testProcessExceptionAbort(): void
    {
        $message = 'message';
        $exception = new AbortException($message);
        $baseCommand = $this->createBaseCommandMock(['writeInfo']);
        $baseCommand->expects(self::once())->method('writeInfo')->with(self::equalTo($message));
        $result = $this->callNonPublicMethod($baseCommand, 'processException', [$exception]);
        self::assertEquals(BaseCommand::SUCCESS, $result);
    }

    /**
     * Tester of processException debug mode implementation
     *
     * @return void
     * @group integration
     * @covers ::processException
     * @group integration
     */
    public function testProcessExceptionDebug(): void
    {
        $debug = true;
        $message = 'message';
        $exception = new Exception($message);
        $trace = $exception->getTraceAsString();
        $errorOutput = $this->createOutputMock(['isDebug']);
        $errorOutput->expects(self::once())->method('isDebug')->willReturn($debug);
        $baseCommand = $this->createBaseCommandMock(['getErrorOutput', 'writeError']);
        $baseCommand->expects(self::any())->method('getErrorOutput')->willReturn($errorOutput);
        $baseCommand->expects(self::once())->method('writeError')->with(self::equalTo([$message, '', $trace]));
        $result = $this->callNonPublicMethod($baseCommand, 'processException', [$exception]);
        self::assertEquals(BaseCommand::FAILURE, $result);
    }

    /**
     * Tester of processCheckLock handle implementation
     *
     * @return void
     * @group integration
     * @covers ::processCheckLock
     * @covers ::processLock
     * @group integration
     */
    public function testProcessCheckLock(): void
    {
        $lockName = 'lockName';
        $baseCommand = $this->createBaseCommandMock(['createLockName']);
        $baseCommand->expects(self::once())->method('createLockName')->willReturn($lockName);
        $this->callNonPublicMethod($baseCommand, 'processCheckLock');
    }

    /**
     * Tester of processCheckLock for exception handle implementation
     *
     * @return void
     * @group integration
     * @covers ::processCheckLock
     * @group integration
     */
    public function testProcessCheckLockException(): void
    {
        $lockName = 'lockName';
        $baseCommand = $this->createBaseCommandMock(['createLockName', 'processLock']);
        $baseCommand->expects(self::once())->method('createLockName')->willReturn($lockName);
        $baseCommand->expects(self::once())->method('processLock')
            ->with(self::equalTo($lockName))
            ->willReturn(false);

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('The command is already running in another process');
        $this->callNonPublicMethod($baseCommand, 'processCheckLock');
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of execute implementation
     *
     * @return void
     * @covers ::execute
     */
    public function testExecute(): void
    {
        $input = $this->createInputMock();
        $output = $this->createOutputMock();
        $result = BaseCommand::SUCCESS;

        $baseCommand = $this->createBaseCommandMock(['setInput', 'setOutput', 'processExecute', 'processCheckLock']);
        $baseCommand->expects(self::once())->method('setInput')->with(self::equalTo($input));
        $baseCommand->expects(self::once())->method('setOutput')->with(self::equalTo($output));
        $baseCommand->expects(self::once())->method('processCheckLock');
        $baseCommand->expects(self::once())->method('processExecute')->willReturn($result);
        self::assertSame($result, $this->callNonPublicMethod($baseCommand, 'execute', [$input, $output]));
    }

    /**
     * Tester of execute exception implementation
     *
     * @return void
     * @covers ::execute
     * @group negative
     */
    public function testExecuteException(): void
    {
        $exception = new Exception();
        $input = $this->createInputMock();
        $output = $this->createOutputMock();
        $result = BaseCommand::FAILURE;

        $baseCommand = $this->createBaseCommandMock([
            'setInput', 'setOutput', 'processException', 'processExecute', 'processCheckLock',
        ]);
        $baseCommand->expects(self::once())->method('setInput')->with(self::equalTo($input));
        $baseCommand->expects(self::once())->method('setOutput')->with(self::equalTo($output));
        $baseCommand->expects(self::once())->method('processCheckLock');
        $baseCommand->expects(self::once())
            ->method('processException')
            ->with(self::equalTo($exception))
            ->willReturn($result);
        $baseCommand->expects(self::once())->method('processExecute')->willThrowException($exception);
        self::assertSame($result, $this->callNonPublicMethod($baseCommand, 'execute', [$input, $output]));
    }

    /**
     * Tester of ErrorOutput getter for getting default Output
     *
     * @return void
     * @covers ::getErrorOutput
     * @depends testGetSetErrorOutput
     */
    public function testGetSetDefaultErrorOutput(): void
    {
        $output = $this->createOutputMock();
        $baseCommand = $this->createBaseCommandMock(['getOutput']);
        $baseCommand->expects(self::once())
            ->method('getOutput')
            ->willReturn($output);
        self::assertSame($output, $this->callNonPublicMethod($baseCommand, 'getErrorOutput'));
    }

    /**
     * Tester of ErrorOutput getter for getting default ConsoleOutput
     *
     * @return void
     * @covers ::getErrorOutput
     * @depends testGetSetErrorOutput
     */
    public function testGetSetDefaultErrorConsoleOutput(): void
    {
        $output = $this->createOutputMock();
        $consoleOutput = $this->createConsoleOutputMock(['getErrorOutput']);
        $consoleOutput->expects(self::once())
            ->method('getErrorOutput')
            ->willReturn($output);
        $baseCommand = $this->createBaseCommandMock(['getOutput']);
        $baseCommand->expects(self::once())
            ->method('getOutput')
            ->willReturn($consoleOutput);
        self::assertSame($output, $this->callNonPublicMethod($baseCommand, 'getErrorOutput'));
    }

    /**
     * Tester of ErrorOutput getter/setter
     *
     * @return void
     * @covers ::getErrorOutput
     * @covers ::setErrorOutput
     */
    public function testGetSetErrorOutput(): void
    {
        $value = $this->createOutputMock();
        $classMock = $this->createBaseCommandMock();
        static::assertSame($classMock, $this->callNonPublicMethod($classMock, 'setErrorOutput', [$value]));
        static::assertSame($value, $this->callNonPublicMethod($classMock, 'getErrorOutput'));
    }

    /**
     * Tester of Input getter/setter
     *
     * @return void
     * @covers ::getInput
     * @covers ::setInput
     */
    public function testGetSetInput(): void
    {
        $value = $this->createInputMock();
        $classMock = $this->createBaseCommandMock();
        static::assertSame($classMock, $this->callNonPublicMethod($classMock, 'setInput', [$value]));
        static::assertSame($value, $this->callNonPublicMethod($classMock, 'getInput'));
    }

    /**
     * Tester of Output getter/setter
     *
     * @return void
     * @covers ::getOutput
     * @covers ::setOutput
     */
    public function testGetSetOutput(): void
    {
        $value = $this->createOutputMock();
        $classMock = $this->createBaseCommandMock();
        static::assertSame($classMock, $this->callNonPublicMethod($classMock, 'setOutput', [$value]));
        static::assertSame($value, $this->callNonPublicMethod($classMock, 'getOutput'));
    }

    /**
     * Tester of StartAt getter/setter implementation
     *
     * @return void
     * @covers ::getStartAt
     * @covers ::setStartAt
     */
    public function testGetSetStartAt(): void
    {
        $startAt = $this->createDateTime();
        $classMock = $this->createBaseCommandMock();
        $this->processTestGetSet($classMock, 'startAt', $startAt);
    }

    /**
     * Tester of write implementation
     *
     * @return void
     * @covers ::write
     */
    public function testWrite(): void
    {
        $messages = 'messages';
        $newLine = false;
        $options = OutputInterface::OUTPUT_NORMAL;
        $output = $this->createOutputMock(['write']);
        $output->expects(self::once())
            ->method('write')
            ->with(self::equalTo($messages), self::equalTo($newLine), self::equalTo($options));
        $baseCommand = $this->createBaseCommandMock(['getOutput', 'processLogMessages']);
        $baseCommand->expects(self::once())
            ->method('processLogMessages')
            ->with(self::equalTo($messages));
        $baseCommand->expects(self::once())
            ->method('getOutput')
            ->willReturn($output);
        $this->callNonPublicMethod($baseCommand, 'write', [$messages, $newLine, $options]);
    }

    /**
     * Tester of writeDebug implementation
     *
     * @return void
     * @covers ::writeDebug
     */
    public function testWriteDebug(): void
    {
        $messages = 'messages';
        $newLine = false;
        $options = OutputInterface::OUTPUT_NORMAL;
        $baseCommand = $this->createBaseCommandMock(['write']);
        $baseCommand->expects(self::once())
            ->method('write')
            ->with(
                self::equalTo($messages),
                self::equalTo($newLine),
                self::equalTo($options | OutputInterface::VERBOSITY_DEBUG),
            );
        $this->callNonPublicMethod($baseCommand, 'writeDebug', [$messages, $newLine, $options]);
    }

    /**
     * Tester of writeError implementation
     *
     * @return void
     * @covers ::writeError
     */
    public function testWriteError(): void
    {
        $messages = 'messages';
        $newLine = false;
        $options = OutputInterface::OUTPUT_NORMAL;
        $errorOutput = $this->createOutputMock(['write']);
        $errorOutput->expects(self::once())
            ->method('write')
            ->with(self::equalTo($messages), self::equalTo($newLine), self::equalTo($options));
        $baseCommand = $this->createBaseCommandMock(['getErrorOutput', 'processLogMessages']);
        $baseCommand->expects(self::once())
            ->method('processLogMessages')
            ->with(self::equalTo($messages));
        $baseCommand->expects(self::once())
            ->method('getErrorOutput')
            ->willReturn($errorOutput);
        $this->callNonPublicMethod($baseCommand, 'writeError', [$messages, $newLine, $options]);
    }

    /**
     * Tester of writeInfo implementation
     *
     * @return void
     * @covers ::writeInfo
     */
    public function testWriteInfo(): void
    {
        $messages = 'messages';
        $newLine = false;
        $options = OutputInterface::OUTPUT_NORMAL;
        $baseCommand = $this->createBaseCommandMock(['write']);
        $baseCommand->expects(self::once())
            ->method('write')
            ->with(
                self::equalTo($messages),
                self::equalTo($newLine),
                self::equalTo($options | OutputInterface::VERBOSITY_VERBOSE),
            );
        $this->callNonPublicMethod($baseCommand, 'writeInfo', [$messages, $newLine, $options]);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * BaseCommand mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @param string[] $addMethods [OPTIONAL] List of mocked method names
     * @return BaseCommand|MockObject
     */
    protected function createBaseCommandMock(array $methods = [], array $addMethods = []): MockObject
    {
        $mock = $this->createMockAbstract(BaseCommand::class, $methods, $addMethods);
        return $mock;
    }

    /**
     * ConsoleOutput mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @param string[] $addMethods [OPTIONAL] List of mocked method names
     * @return ConsoleOutputInterface|MockObject
     */
    protected function createConsoleOutputMock(array $methods = [], array $addMethods = []): MockObject
    {
        $mock = $this->createMockAbstract(ConsoleOutputInterface::class, $methods, $addMethods);
        return $mock;
    }

    /**
     * DateTime factory
     *
     * @param string $dateTime [OPTIONAL]
     * @param DateTimeZone|null $timezone [OPTIONAL]
     * @return DateTime
     */
    protected function createDateTime(string $dateTime = 'now', ?DateTimeZone $timezone = null): DateTime
    {
        $object = new DateTime($dateTime, $timezone); // @phpstan-ignore missingType.checkedException
        return $object;
    }

    /**
     * Input mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @param string[] $addMethods [OPTIONAL] List of mocked method names
     * @return InputInterface|MockObject
     */
    protected function createInputMock(array $methods = [], array $addMethods = []): MockObject
    {
        $mock = $this->createMockAbstract(InputInterface::class, $methods, $addMethods);
        return $mock;
    }

    /**
     * Output mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @param string[] $addMethods [OPTIONAL] List of mocked method names
     * @return OutputInterface|MockObject
     */
    protected function createOutputMock(array $methods = [], array $addMethods = []): MockObject
    {
        $mock = $this->createMockAbstract(OutputInterface::class, $methods, $addMethods);
        return $mock;
    }

    // </editor-fold>
}
