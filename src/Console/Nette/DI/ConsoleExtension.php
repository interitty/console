<?php

declare(strict_types=1);

namespace Interitty\Console\Nette\DI;

use Interitty\Console\Application;
use Interitty\Console\LazyCommandLoader;
use Interitty\DI\CompilerExtension;
use Interitty\Exceptions\Exceptions;
use Nette\DI\Definitions\Definition;
use Nette\DI\ServiceCreationException;
use Nette\Utils\Arrays;
use Symfony\Component\Console\Command\Command;

use function call_user_func;
use function is_array;
use function is_string;

class ConsoleExtension extends CompilerExtension
{
    /** All available provider name constants */
    public const string PROVIDER_CONSOLE_APPLICATION = 'consoleApplication';
    public const string PROVIDER_LAZY_COMMAND_LOADER = 'lazyCommandLoader';

    /** @var bool */
    protected bool $consoleMode;

    /**
     * Constructor
     *
     * @param bool $consoleMode
     * @return void
     */
    public function __construct(bool $consoleMode = true)
    {
        $this->setConsoleMode($consoleMode);
    }

    /**
     * @inheritdoc
     */
    public function beforeCompile(): void
    {
        if ($this->isConsoleMode() === true) {
            $this->setupLazyComandLoader();
            $this->setupConsoleApplication();
        }
    }

    /**
     * Console application setup
     *
     * @return void
     */
    protected function setupConsoleApplication(): void
    {
        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix(self::PROVIDER_CONSOLE_APPLICATION))
            ->setFactory(Application::class)
            ->addSetup('setCommandLoader', [$this->prefix('@' . self::PROVIDER_LAZY_COMMAND_LOADER)]);
    }

    /**
     * Lazy command loader setup
     *
     * @return void
     */
    protected function setupLazyComandLoader(): void
    {
        $commandMap = $this->getComandMap();
        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix(self::PROVIDER_LAZY_COMMAND_LOADER))
            ->setFactory(LazyCommandLoader::class)
            ->setArguments(['@container', $commandMap]);
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Command map getter
     *
     * @phpstan-return array<string, string>
     */
    protected function getComandMap(): array
    {
        $builder = $this->getContainerBuilder();
        $commands = $builder->findByType(Command::class);
        $commandMap = [];
        foreach ($commands as $serviceName => $service) {
            /** @phpstan-var class-string<Command> $commandClass */
            $commandClass = $service->getType();
            $commandName = $this->getCommandTagName($service) ?? $this->getCommandAttributeName($commandClass);
            $commandMap[$commandName] = $serviceName;
        }
        return $commandMap;
    }

    /**
     * Command name by AsCommand attribute getter
     *
     * @phpstan-param class-string<Command> $commandClass
     * @return string
     * @throws ServiceCreationException
     */
    protected function getCommandAttributeName(string $commandClass): string
    {
        $commandName = call_user_func([$commandClass, 'getDefaultName']);
        if ($commandName === null) {
            throw Exceptions::extend(ServiceCreationException::class)
                    ->setMessage('Command ":class" missing #[AsCommand] attribute')
                    ->addData('class', $commandClass);
        }
        return $commandName;
    }

    /**
     * Command name by service tag name getter
     *
     * @param Definition $service
     * @return string|null
     */
    protected function getCommandTagName(Definition $service): ?string
    {
        $tags = $service->getTags();
        $commandName = null;
        if (isset($tags['console.command'])) {
            if (is_string($tags['console.command'])) {
                $commandName = $tags['console.command'];
            } elseif (is_array($tags['console.command'])) {
                $commandName = Arrays::get($tags['console.command'], 'name', null);
            }
        }
        /** @phpstan-var string|null $commandName */
        return $commandName;
    }

    /**
     * ConsoleMode checker
     *
     * @return bool
     */
    protected function isConsoleMode(): bool
    {
        return $this->consoleMode;
    }

    /**
     * ConsoleMode setter
     *
     * @param bool $consoleMode
     * @return static Provides fluent interface
     */
    protected function setConsoleMode(bool $consoleMode): static
    {
        $this->consoleMode = $consoleMode;
        return $this;
    }

    // </editor-fold>
}
