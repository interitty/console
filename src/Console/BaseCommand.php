<?php

declare(strict_types=1);

namespace Interitty\Console;

use DateTimeInterface;
use Interitty\Exceptions\Exceptions;
use Interitty\Utils\DateTime;
use LogicException;
use Nette\Application\AbortException;
use Nette\Utils\Json;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

use function hash;
use function is_iterable;
use function iterator_to_array;

abstract class BaseCommand extends Command
{
    use LockableTrait;

    /** @var bool */
    protected static bool $isExclusive = true;

    /** @var bool */
    protected static bool $isLogTimestamped = false;

    /** @var OutputInterface */
    protected OutputInterface $errorOutput;

    /** @var InputInterface */
    protected InputInterface $input;

    /** @var OutputInterface */
    protected OutputInterface $output;

    /** @var DateTimeInterface */
    protected DateTimeInterface $startAt;

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $now = $this->createDateTimeNow();
            $this->setStartAt($now);
            $this->setInput($input);
            $this->setOutput($output);
            $this->processCheckLock();
            $result = $this->processExecute();
            $this->release();
        } catch (Throwable $exception) {
            $result = $this->processException($exception);
        }
        return $result;
    }

    /**
     * Exception processor
     *
     * @param Throwable $exception
     * @return int One of the self::FAILURE, self::INVALID, self::SUCCESS constants
     */
    protected function processException(Throwable $exception): int
    {
        if ($exception instanceof AbortException) {
            $this->writeInfo($exception->getMessage());
            $result = self::SUCCESS;
        } else {
            $error = [$exception->getMessage()];
            if ($this->getErrorOutput()->isDebug() === true) {
                $error[] = '';
                $error[] = $exception->getTraceAsString();
            }
            $this->writeError($error);
            $result = self::FAILURE;
        }
        return $result;
    }

    /**
     * Execute processor
     *
     * @return int One of the self::FAILURE, self::INVALID, self::SUCCESS constants
     * @throws Throwable
     */
    abstract protected function processExecute(): int;

    /**
     * Check lock processor
     *
     * @return void
     * @throws LogicException
     */
    protected function processCheckLock(): void
    {
        $lockName = static::$isExclusive ? $this->createLockName() : false;
        if (($lockName !== false) && ($this->processLock($lockName)) !== true) {
            throw Exceptions::extend(LogicException::class)
                    ->setMessage('The command is already running in another process');
        }
    }

    /**
     * Check long running commands processor
     *
     * @return void
     */
    protected function processCheckLongRunning(): void
    {
        $now = $this->createDateTimeNow()->format('Y-m-d');
        $startAt = $this->getStartAt()->format('Y-m-d');
        if ($now !== $startAt) {
            throw Exceptions::extend(AbortException::class)
                    ->setMessage('Command was started yesterday');
        }
    }

    /**
     * Lock processor
     *
     * @param string|null $lockName
     * @return bool
     */
    protected function processLock(?string $lockName): bool
    {
        $lock = $this->lock($lockName);
        return $lock;
    }

    /**
     * Log messages processor
     *
     * @param string|iterable<string> $messages
     * @return void
     */
    protected function processLogMessages(string|iterable &$messages): void
    {
        if (static::$isLogTimestamped === true) {
            $timestamp = '';
            if ($this->getOutput()->isDebug() === true) {
                $timestamp = '<fg=gray>' . $this->createDateTimeNow()->format('Y-m-d h:i:s') . '</> ';
            }

            $messages = is_iterable($messages) ? iterator_to_array($messages) : [$messages];
            foreach ($messages as $key => $message) {
                $messages[$key] = $timestamp . $message;
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * DateTime now factory
     *
     * @return DateTime
     */
    protected function createDateTimeNow(): DateTime
    {
        $dateTime = DateTime::from('now'); // @phpstan-ignore missingType.checkedException
        return $dateTime;
    }

    /**
     * LockName factory
     *
     * @return string
     */
    protected function createLockName(): string
    {
        $options = $this->getInput()->getOptions();
        $lockData = ['name' => $this->getName(), 'options' => $options];
        $lockName = hash('md5', Json::encode($lockData));
        return $lockName;
    }

    /**
     * ProgressBar factory
     *
     * @param int $max [OPTIONAL]
     * @param float $minSeconds [OPTIONAL] Minimal seconds between redraws
     * @return ProgressBar
     */
    protected function createProgressBar(int $max = 0, float $minSeconds = 1 / 25)
    {
        $output = $this->getOutput();
        $progressBar = new ProgressBar($output, $max, $minSeconds);
        return $progressBar;
    }

    /**
     * Writes a message to the standard output
     *
     * @param string|iterable<string> $messages The message as an iterable of strings or a single string
     * @param bool $newLine Whether to add a newline
     * @param int $options A bitmask of OutputInterface::OUTPUT_* or OutputInterface::VERBOSITY_* constants
     * @return void
     */
    protected function write(string|iterable $messages, bool $newLine = true, int $options = 0): void
    {
        $this->processLogMessages($messages);
        $this->getOutput()->write($messages, $newLine, $options);
    }

    /**
     * Writes a debug message to the standard output
     *
     * @param string|iterable<string> $messages The message as an iterable of strings or a single string
     * @param bool $newLine Whether to add a newline
     * @param int $options A bitmask of OutputInterface::OUTPUT_*
     * @return void
     */
    protected function writeDebug(string|iterable $messages, bool $newLine = true, int $options = 0): void
    {
        $this->write($messages, $newLine, $options | OutputInterface::VERBOSITY_DEBUG);
    }

    /**
     * Writes a message to the standard error output
     *
     * @param string|iterable<string> $messages The message as an iterable of strings or a single string
     * @param bool $newLine Whether to add a newline
     * @param int $options A bitmask of OutputInterface::OUTPUT_* or OutputInterface::VERBOSITY_* constants
     * @return void
     */
    protected function writeError(string|iterable $messages, bool $newLine = true, int $options = 0): void
    {
        $this->processLogMessages($messages);
        $this->getErrorOutput()->write($messages, $newLine, $options);
    }

    /**
     * Writes an info message to the standard output
     *
     * @param string|iterable<string> $messages The message as an iterable of strings or a single string
     * @param bool $newLine Whether to add a newline
     * @param int $options A bitmask of OutputInterface::OUTPUT_*
     * @return void
     */
    protected function writeInfo(string|iterable $messages, bool $newLine = true, int $options = 0): void
    {
        $this->write($messages, $newLine, $options | OutputInterface::VERBOSITY_VERBOSE);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * ErrorOutput getter
     *
     * @return OutputInterface
     */
    protected function getErrorOutput(): OutputInterface
    {
        if (isset($this->errorOutput) === false) {
            $output = $this->getOutput();
            $errorOutput = $output instanceof ConsoleOutputInterface ? $output->getErrorOutput() : $output;
            $this->setErrorOutput($errorOutput);
        }
        return $this->errorOutput;
    }

    /**
     * ErrorOutput setter
     *
     * @param OutputInterface $errorOutput
     * @return static Provides fluent interface
     */
    protected function setErrorOutput(OutputInterface $errorOutput)
    {
        $this->errorOutput = $errorOutput;
        return $this;
    }

    /**
     * Input getter
     *
     * @return InputInterface
     */
    protected function getInput(): InputInterface
    {
        return $this->input;
    }

    /**
     * Input setter
     *
     * @param InputInterface $input
     * @return static Provides fluent interface
     */
    protected function setInput(InputInterface $input)
    {
        $this->input = $input;
        return $this;
    }

    /**
     * Output getter
     *
     * @return OutputInterface
     */
    protected function getOutput(): OutputInterface
    {
        return $this->output;
    }

    /**
     * Output setter
     *
     * @param OutputInterface $output
     * @return static Provides fluent interface
     */
    protected function setOutput(OutputInterface $output)
    {
        $this->output = $output;
        return $this;
    }

    /**
     * StartAt getter
     *
     * @return DateTimeInterface
     */
    protected function getStartAt(): DateTimeInterface
    {
        return $this->startAt;
    }

    /**
     * StartAt setter
     *
     * @param DateTimeInterface $startAt
     * @return static Provides fluent interface
     */
    protected function setStartAt(DateTimeInterface $startAt): static
    {
        $this->startAt = $startAt;
        return $this;
    }

    // </editor-fold>
}
