<?php

declare(strict_types=1);

namespace Interitty\Console;

use Interitty\PhpUnit\BaseTestCase;
use Nette\DI\Container;
use Nette\Utils\AssertionException;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Console\Command\Command;

/**
 * @coversDefaultClass Interitty\Console\LazyCommandLoader
 */
class LazyCommandLoaderTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of Constructor implementation
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstructor(): void
    {
        $commandMap = [
            'nameOne' => 'CommandServiceName',
            'nameTwo' => 'AnotherCommandServiceName',
        ];
        $container = $this->createContainerMock();
        $loader = $this->createLazyCommandLoaderMock(['setContainer', 'setCommandMap']);
        $loader->expects(self::once())->method('setContainer')->with(self::equalTo($container));
        $loader->expects(self::once())->method('setCommandMap')->with(self::equalTo($commandMap));
        $loader->__construct($container, $commandMap);
    }

    /**
     * Tester of command getter implementation
     *
     * @return void
     * @covers ::get
     */
    public function testGet(): void
    {
        $name = 'name';
        $serviceName = 'comandName';
        $command = $this->createCommandMock(['setName']);
        $command->expects(self::once())->method('setName')->with(self::equalTo($name));
        $commandMap = [$name => $serviceName];
        $container = $this->createContainerMock(['getService']);
        $container->expects(self::once())
            ->method('getService')
            ->with(self::equalTo($serviceName))
            ->willReturn($command);
        $loader = $this->createLazyCommandLoaderMock(['getCommandMap', 'getContainer', 'has']);
        $loader->expects(self::once())->method('has')->with(self::equalTo($name))->willReturn(true);
        $loader->expects(self::once())->method('getCommandMap')->willReturn($commandMap);
        $loader->expects(self::once())->method('getContainer')->willReturn($container);
        self::assertSame($command, $loader->get($name));
    }

    /**
     * Tester of command getter for exception handler implementation
     *
     * @return void
     * @covers ::get
     * @group negative
     */
    public function testGetException(): void
    {
        $name = 'name';
        $loader = $this->createLazyCommandLoaderMock(['getCommandMap', 'getContainer', 'has']);
        $loader->expects(self::once())->method('has')->with(self::equalTo($name))->willReturn(false);
        $loader->expects(self::never())->method('getCommandMap');
        $loader->expects(self::never())->method('getContainer');
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('Command ":name" does not exist');
        $this->expectExceptionData(['name' => $name]);
        $loader->get($name);
    }

    /**
     * Tester of Command checker implementation
     *
     * @return void
     * @covers ::has
     */
    public function testHas(): void
    {
        $nameOne = 'nameOne';
        $nameTwo = 'nameTwo';
        $nameMissing = 'nameMissing';
        $comandMap = [
            $nameOne => 'CommandServiceName',
            $nameTwo => 'AnotherCommandServiceName',
        ];
        $loader = $this->createLazyCommandLoaderMock(['getCommandMap']);
        $loader->expects(self::exactly(3))->method('getCommandMap')->willReturn($comandMap);
        self::assertTrue($loader->has($nameOne));
        self::assertTrue($loader->has($nameTwo));
        self::assertFalse($loader->has($nameMissing));
    }

    /**
     * Tester of CommandMap getter/setter implementation
     *
     * @return void
     * @covers ::getCommandMap
     * @covers ::setCommandMap
     */
    public function testGetSetCommandMap(): void
    {
        $commandMap = [
            'nameOne' => 'CommandServiceName',
            'nameTwo' => 'AnotherCommandServiceName',
        ];
        $this->processTestGetSet(LazyCommandLoader::class, 'commandMap', $commandMap);
    }

    /**
     * Tester of Container getter/setter implementation
     *
     * @return void
     * @covers ::getContainer
     * @covers ::setContainer
     */
    public function testGetSetContainer(): void
    {
        $container = $this->createContainerMock();
        $this->processTestGetSet(LazyCommandLoader::class, 'container', $container);
    }

    /**
     * Tester of names getter implementation
     *
     * @return void
     * @covers ::getNames
     */
    public function testGetNames(): void
    {
        $nameOne = 'nameOne';
        $nameTwo = 'nameTwo';
        $comandMap = [
            $nameOne => 'CommandServiceName',
            $nameTwo => 'AnotherCommandServiceName',
        ];
        $loader = $this->createLazyCommandLoaderMock(['getCommandMap']);
        $loader->expects(self::once())->method('getCommandMap')->willReturn($comandMap);
        self::assertSame([$nameOne, $nameTwo], $loader->getNames());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Command mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Command&MockObject
     */
    protected function createCommandMock(array $methods = []): Command&MockObject
    {
        $mock = $this->createPartialMock(Command::class, $methods);
        return $mock;
    }

    /**
     * LazyCommandLoader mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return LazyCommandLoader&MockObject
     */
    protected function createLazyCommandLoaderMock(array $methods = []): LazyCommandLoader&MockObject
    {
        $mock = $this->createPartialMock(LazyCommandLoader::class, $methods);
        return $mock;
    }

    /**
     * Container mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Container&MockObject
     */
    protected function createContainerMock(array $methods = []): Container&MockObject
    {
        $mock = $this->createPartialMock(Container::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
