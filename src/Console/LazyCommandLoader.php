<?php

declare(strict_types=1);

namespace Interitty\Console;

use Interitty\Exceptions\Exceptions;
use Nette\DI\Container;
use Nette\Utils\AssertionException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\CommandLoader\CommandLoaderInterface;

use function array_key_exists;
use function array_keys;
use function assert;

class LazyCommandLoader implements CommandLoaderInterface
{
    /** @phpstan-var array<string, string> $commandMap */
    protected array $commandMap;

    /** @var Container */
    protected Container $container;

    /**
     * Constructor
     *
     * @phpstan-param array<string, string> $commandMap
     * @return void
     */
    public function __construct(Container $container, array $commandMap)
    {
        $this->setContainer($container);
        $this->setCommandMap($commandMap);
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Command getter
     *
     * @param string $name
     * @return Command
     */
    public function get(string $name): Command
    {
        if ($this->has($name) !== true) {
            throw Exceptions::extend(AssertionException::class)
                    ->setMessage('Command ":name" does not exist')
                    ->addData('name', $name);
        }
        $commandName = $this->getCommandMap()[$name];
        $command = $this->getContainer()->getService($commandName);
        assert($command instanceof Command);
        $command->setName($name);
        return $command;
    }

    /**
     * Command exists checker
     *
     * @param string $name
     * @return bool
     */
    public function has(string $name): bool
    {
        $hasCommand = array_key_exists($name, $this->getCommandMap());
        return $hasCommand;
    }

    /**
     * Command map getter
     *
     * @phpstan-return array<string, string>
     */
    protected function getCommandMap(): array
    {
        return $this->commandMap;
    }

    /**
     * Command map setter
     *
     * @phpstan-param array<string, string> $commandMap
     * @return static Provides fluent interface
     */
    protected function setCommandMap(array $commandMap): static
    {
        $this->commandMap = $commandMap;
        return $this;
    }

    /**
     * Container getter
     *
     * @return Container
     */
    protected function getContainer(): Container
    {
        return $this->container;
    }

    /**
     * Container setter
     *
     * @param Container $container
     * @return static Provides fluent interface
     */
    protected function setContainer(Container $container): static
    {
        $this->container = $container;
        return $this;
    }

    /**
     * Names getter
     *
     * @return string[]
     */
    public function getNames(): array
    {
        $commandNames = array_keys($this->getCommandMap());
        return $commandNames;
    }

    // </editor-fold>
}
